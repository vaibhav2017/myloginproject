import React, { Component } from 'react';
import { View, Text } from 'react-native';

export default class Welcome extends Component{

    render(){
        return(
            <View>
                <Text>Welcome to React Native</Text>
            </View>
        )
    }
}