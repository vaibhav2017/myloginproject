class MainActivity extends Component {

    static navigationOptions =
      {
         title: 'MainActivity',
      };
    
      FunctionToOpenSecondActivity = () =>
      {
         this.props.navigation.navigate('Second');
         
      }
    
      render()
      {
         return(
            <View style = { styles.MainContainer }>
    
              <View style={{marginBottom: 20}}>
    
               <Text style = { styles.TextStyle }> This is MainActivity </Text>
    
              </View>
    
               <Button onPress = { this.FunctionToOpenSecondActivity } title = 'Click Here To Open Second Activity'/>
             
            </View>
         );
      }
    }